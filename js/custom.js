"use strict";

window.onload = function () {

	/* Clock */
	var clock;
	var text = '';
	var clockId = null;

	startClock();

	function startClock () {
		var formatClock = getAttr();

		clearInterval(clockId);

		if (formatClock === 'full') {
			clockId = setInterval(fullClock, 100);
		}

		if (formatClock === 'short') {
			clockId = setInterval(shortClock, 100);
		}
	}

	function getAttr () {
		var elem = document.getElementById('clock');
		var elemAttr = elem.getAttribute('data-format');

		return elemAttr;
	}

	function fullClock () {
		var date = new Date();
		var h = addZero( date.getHours() );
		var m = addZero( date.getMinutes() );
		var s = addZero( date.getSeconds() );

		text = h + ':' + m + ':' + s;
		document.getElementById('clock').innerHTML = text;
	}

	function shortClock () {
		var date = new Date();
		var h = addZero( date.getHours() );
		var m = addZero( date.getMinutes() );

		text = h + ':' + m;
		document.getElementById('clock').innerHTML = text;
	}

	function addZero (elem) {
		if (elem < 10) {
			elem = '0' + elem;
		}

		return elem;
	}

	function changeFormatClock () {
		var elem = document.getElementById('clock');
		var elemAttr = elem.getAttribute('data-format');

		if (elemAttr === 'full') {
			elem.setAttribute('data-format', 'short');
		} else {
			elem.setAttribute('data-format', 'full');
		}
	}

	clock = document.getElementById('clock');
	
	clock.onclick = function () {
		changeFormatClock();
		startClock();
	};


	/* Sum */
	function sum (a) {
		return function (b) {
			return a + b;
		};
	}

	console.log( sum(1)(2) );


	/* Output to the console from 1 to 10  -  first variant*/
	for (var i = 0; i < 10; i++) {
		setTimeout( closuerFunc(i), 0 );
	}

	function closuerFunc (index) {
		return function () {
			console.log(index + 1);
		};
	}

	/* Second variant */
	for (var i = 0; i < 10; i++) {
		setTimeout( function (index) {
			console.log(index + 1);
		}, 0, i );
	}

	/* Third variant */
	for (var i = 0; i < 10; i++) {
		(function () {
			var index = i;
			
			setTimeout( function () {
				console.log(index + 1);
			}, 0 );
		})();
	}	
};